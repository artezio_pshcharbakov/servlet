package by.bsu.filter;

import by.bsu.wrappers.GenericResponseWrapper;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

@WebFilter("/any/*")
public class GerenicFilter implements Filter {

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        OutputStream out = response.getOutputStream();
        out.write("<HR>PRE<HR>".getBytes());
        GenericResponseWrapper wrapper = new GenericResponseWrapper((HttpServletResponse) response);
        chain.doFilter(request, wrapper);
        out.write(wrapper.getData());
        out.write("<HR>POST<HR>".getBytes());
        out.close();
    }
}
