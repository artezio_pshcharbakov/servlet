package by.bsu.servlet;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class OddDigits extends SimpleTagSupport {

    private String array;

    public void setArray(String array) {
        this.array = array;
    }

    @Override
    public void doTag() throws JspException, IOException {
        if (!array.isEmpty()) {
            String[] res = array.split(",");
            List<Integer> result = new ArrayList<>(res.length / 2);
            for (String s : res) {
                int m = Integer.valueOf(s);
                if (m % 2 == 1) {
                    result.add(m);
                }
            }
            String print = result.stream().map(String::valueOf).collect(Collectors.joining(" "));
            JspWriter out = getJspContext().getOut();
            out.println(print);
        }
    }
}
