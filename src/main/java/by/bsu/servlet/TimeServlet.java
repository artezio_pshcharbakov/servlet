package by.bsu.servlet;

import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@WebServlet(urlPatterns = "/timeaction",
        initParams = @WebInitParam(name = "mail.smtps.host", value = "smtp.gmail.com"))
public class TimeServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        GregorianCalendar gc = new GregorianCalendar();
        String time = req.getParameter("time");
        float delta = ((float) (gc.getTimeInMillis() - Long.parseLong(time))) / 1_000;
        req.setAttribute("delta", delta);
//        req.getSession().getAttributeNames()

        List<String> contextParamNames = getParamInitNames(getServletContext().getInitParameterNames());
        List<String> servletParamNames = getParamInitNames(getServletConfig().getInitParameterNames());
        req.setAttribute("initParams", contextParamNames);
        req.setAttribute("SCinitParams", servletParamNames);
        req.setAttribute("PathInfo", req.getPathInfo());
        req.setAttribute("Protocol", req.getProtocol());
        req.setAttribute("PrincipalName", req.getUserPrincipal());
        req.setAttribute("RemoteUser", req.getRemoteUser());
        req.setAttribute("ServerName", req.getServerName());
        req.setAttribute("AuthType", req.getAuthType());
        req.setAttribute("PathTranslated", req.getPathTranslated());
        req.setAttribute("ServerPort", req.getServerPort());
        req.setAttribute("RemotePort", req.getRemotePort());
        req.setAttribute("RemoteAddr", req.getRemoteAddr());
        req.setAttribute("LocalAddr", req.getLocalAddr());
        req.setAttribute("CharacterEncoding", req.getCharacterEncoding());
        req.setAttribute("ContextPath", req.getContextPath());
        req.setAttribute("Method", req.getMethod());
        req.setAttribute("QueryString", req.getQueryString());
        req.setAttribute("RequestURI", req.getRequestURI());
        req.setAttribute("RequestURL", req.getRequestURL().toString());
        req.setAttribute("ContentType", req.getContentType());
        req.setAttribute("Scheme", req.getScheme());
        req.getRequestDispatcher("/jsp/result.jsp").forward(req, resp);

        ServletRegistration.Dynamic how = getServletContext().addServlet(this.getClass().getName(), this);
    }

    private List<String> getParamInitNames(Enumeration<String> contextInitParams) {
        List<String> contextParamNames = new ArrayList<>();
        if (contextInitParams.hasMoreElements()) {
            contextParamNames.add(contextInitParams.nextElement());
        }
        return contextParamNames;
    }
}
