package by.bsu.old;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/show/string")
public class ShowString  extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String send = req.getParameter("str");
        PrintWriter out = resp.getWriter();
        out.println("<!DOCTYPE HTML>");
        out.println("<html>");
        out.println("<head>");
        out.println("<meta charset='utf-8'>");
        out.println("</head>");
        out.println("<body>");
        if (send.isEmpty()) {
            out.print("<h2>You didn't enter anything</h2>");
        } else {
            out.print("<h2>You entered: </h2>" + send);
        }
        out.println("<form name='name' method='get' action='/preparing/oldest'>");
        out.println("<p><input type='submit' value='back'>");
        out.println("</p>");
        out.println("</form>");
        out.println("</body>");
        out.println("</html>");
    }
}
