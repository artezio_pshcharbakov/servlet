package by.bsu.old;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/oldest")
public class Servlet96 extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        out.println("<!DOCTYPE HTML>");
        out.println("<html>");
        out.println("<head>");
        out.println("<meta charset='utf-8'>");
        out.println("</head>");
        out.println("<body>");
        out.println("<form name='name' method='post' action='show/string'>");
        out.println("<p><b>Enter string:</b><br>");
        out.println("<input name='str' type='text' size='40'>");
        out.println("</p>");
        out.println("<p><input type='submit' value='Send'>");
        out.println("</form>");
        out.println("</body>");
        out.println("</html>");
    }
}
