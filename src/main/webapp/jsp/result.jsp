<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <title>JSP Timing</title>
    </head>
    <body>
        <h5>Прошло время: ${delta}</h5>
         <h5>Context Initial params: ${initParams}</h5>
         <h5>ServletConfig Params: ${SCinitParams}</h5>
         <h5>servletInfo: ${servletInfo}</h5>
          <h5>PathInfo: ${PathInfo}</h5>
          <h5>Protocol: ${Protocol}</h5>
          <h5>RemoteUser: ${RemoteUser}</h5>
          <h5>ServerName: ${ServerName}</h5>
          <h5>AuthType: ${AuthType}</h5>
          <h5>PathTranslated: ${PathTranslated}</h5>
          <h5>ServerPort: ${ServerPort}</h5>
          <h5>RemotePort: ${RemotePort}</h5>
          <h5>RemoteAddr: ${RemoteAddr}</h5>
          <h5>LocalAddr: ${LocalAddr}</h5>
          <h5>CharacterEncoding: ${CharacterEncoding}</h5>
          <h5>ContextPath: ${ContextPath}</h5>
          <h5>Method: ${Method}</h5>
          <h5>QueryString: ${QueryString}</h5>
          <h5>RequestURI: ${RequestURI}</h5>
          <h5>RequestURL: ${RequestURL}</h5>
          <h5>ContentType: ${ContentType}</h5>
          <h5>Scheme: ${Scheme}</h5>
    </body>
</html>