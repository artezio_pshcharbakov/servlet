<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <title>JSP Timing</title>
    </head>
    <body>
       <h5>Page Context: ${pageContext}</h5>
       <h5>Servlet Context: ${servletContext}</h5>
       <h5>param: ${param}</h5>
       <h5>paramValues: ${paramValues}</h5>
       <h5>request: ${request}</h5>
       <h5>config: ${config}</h5>
       <h5>Page: ${page}</h5>
    </body>
</html>