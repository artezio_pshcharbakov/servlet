<%@ taglib prefix = "mission" uri = "/WEB-INF/custom.tld"%>
<html>
    <head>
        <meta charset='utf-8'>
        <script type="text/javascript">
            var words = [];
                function writeInput() {
                    var inputString = document.getElementById('str');
                    var result = document.getElementById('result');
                    words.push(inputString.value);
                    words.forEach(function(item, index, array) {
                                   console.log(item, index);
                                 });
                    result.value = words;
                    inputString.value='';
                }
            </script>
    </head>
    <body>
        <form id='form' method='post' action='/preparing/jsp/form.jsp'>
            <p>
                <b>Enter string on JSP:</b><br>
                <input id='str' name='str' type='text' size='15'>
                <input id='result' name='result' type='hidden' size='15'>
            </p>
            <p>
                 <input type='submit' value='Show odds' name='Send'/>
            </p>
        </form>
        <button onclick="writeInput();">Add</button>
        <c:if test="${not empty param.Send}">
            <p>Odds: <mission:complete array="${param.result}"/></p>
        </c:if>
    </body>
</html>